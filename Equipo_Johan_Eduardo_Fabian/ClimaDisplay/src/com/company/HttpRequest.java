package com.company;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class HttpRequest
{
    private URL Url;

    public HttpRequest(String url) throws Exception
    {
        this.Url = new URL(url);
    }

    public String getRequest() throws Exception
    {
        int ResponseCode;
        BufferedReader InputReader;
        StringBuffer Response = new StringBuffer();
        String Line;

        HttpURLConnection Connection = (HttpURLConnection)Url.openConnection();
        Connection.setRequestMethod("GET");
        Connection.setRequestProperty("User-Agent", "ClimateDisplayApp");
        ResponseCode = Connection.getResponseCode();

        System.out.println("GET Request to " + Url);
        System.out.println("Response code: " + ResponseCode);

        InputReader = new BufferedReader(new InputStreamReader(Connection.getInputStream()));
        while((Line = InputReader.readLine()) != null)
            Response.append(Line);
        InputReader.close();

        return Response.toString();
    }

}
