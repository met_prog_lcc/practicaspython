package com.company;

import javax.swing.*;
import org.json.*;

import java.awt.*;
import java.awt.Graphics.*;

import java.io.FileReader;

public class Main extends JFrame{
    static JFrame MainWindowFrame = new JFrame();
    public static void main(String[] args) throws Exception{
	    MainWindowFrame.setLayout(null);
	    MainWindowFrame.setSize(600, 600);

	    System.out.println(System.getProperty("user.dir"));
	    JSONObject ListaCiudades = new JSONObject(new FileReader(System.getProperty("user.dir") + "\\src\\com\\company\\city.list.json"));
	    JSONObject Anomalias = new JSONObject(new FileReader(System.getProperty("user.dir") + "\\src\\com\\company\\climateChange.json"));

	    HttpRequest HttpReq = new HttpRequest("http://api.openweathermap.org/data/2.5/weather?id=4006702&APPID=6ae97362910dce12c00211aafc3a5742");

	    String Response = HttpReq.getRequest();

	    System.out.println("Response: " + Response);

        JSONObject ResponseObject = new JSONObject(Response);
        String Clima = ResponseObject.getJSONArray("weather").getJSONObject(0).getString("description");
        String Temperatura = String.valueOf(ResponseObject.getJSONObject("main").getInt("temp"));
        String Ciudad = ResponseObject.getString("name");
        String Humedad = String.valueOf(ResponseObject.getJSONObject("main").getInt("humidity"));
        String TempMin = String.valueOf(ResponseObject.getJSONObject("main").getInt("temp_min"));
        String TempMax = String.valueOf(ResponseObject.getJSONObject("main").getInt("temp_max"));
        JLabel LabelClima = new JLabel("Pronostico: " + Clima);
        JLabel LabelTemp  = new JLabel("Temperatura: " + ((Integer.parseInt(Temperatura)-32)/(1.8f)) + "º C");
        JLabel LabelCiudad = new JLabel("Ciudad: " + Ciudad);
        JLabel LabelHumedad = new JLabel("Humedad: " + Humedad + "%");
        JLabel LabelTempMin = new JLabel("Temperatura Minima: " + ((Integer.parseInt(TempMin)-32)/(1.8f)) + "º C");
        JLabel LabelTempMax = new JLabel("Temperatura Maxima: " + ((Integer.parseInt(TempMax)-32)/(1.8f)) + "º C");
        LabelClima.setBounds(10, 10, 600, 200);
        LabelTemp.setBounds(200, 10, 600, 200);
        LabelCiudad.setBounds(390, 10, 600, 200);
        LabelHumedad.setBounds(10, 100, 600, 200);
        LabelTempMin.setBounds(150, 100, 600, 200);
        LabelTempMax.setBounds(360, 100, 600, 200);
        MainWindowFrame.add(LabelClima);
        MainWindowFrame.add(LabelTemp);
        MainWindowFrame.add(LabelCiudad);
        MainWindowFrame.add(LabelHumedad);
        MainWindowFrame.add(LabelTempMax);
        MainWindowFrame.add(LabelTempMin);
        MainWindowFrame.setVisible(true);






    }
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        g.setColor(Color.BLACK);
        g.drawLine(10, 30, 100, 100);
    }
}
